module Main exposing (..)

import Browser
import Element exposing (..)
import Element.Background as Background
import Element.Border as Border
import Element.Events as Events
import Element.Font as Font
import Element.Input as Input
import Html exposing (Html, div)
import Html.Attributes exposing (src)
import Http exposing (Error)
import Json.Decode as Decode
import Json.Decode.Pipeline exposing (custom, required)
import List.Extra



---- MODEL ----


type alias Model =
    { posts : List Post
    , visiblePosts : List Post
    , listOfListPosts : List (List Post)
    , users : List User
    , comments : List Comment
    , currentPage : Page
    , lastError : String
    , subListIndex : Int
    }


type Page
    = PostsPage
    | UsersPage
    | PostCommentsPage
    | EmptyPage
    | LoadingPage


type Msg
    = NoOp
    | GetPosts
    | GotPosts (Result Http.Error (List Post))
    | GetUsers
    | GotUsers (Result Http.Error (List User))
    | LoadPostComments Int
    | GotPostComments (Result Http.Error (List Comment))
    | IncrementPagination
    | DecrementPagination


type alias Flags =
    ()



-- POST TYPES


type alias Post =
    { id : Int
    , userId : Int
    , title : String
    , body : String
    }


type alias Comment =
    { id : Int
    , postId : Int
    , name : String
    , email : String
    , body : String
    }



-- USER TYPES


type alias User =
    { id : Int
    , name : String
    , username : String
    , email : String
    , phone : String
    , website : String
    , address : Address
    , company : Company
    }


type alias Address =
    { street : String
    , suite : String
    , city : String
    , zipcode : String
    , geo : GeoCoords
    }


type alias GeoCoords =
    { lat : String
    , lng : String
    }


type alias Company =
    { name : String
    , catchPhrase : String
    , bs : String
    }




-- INIT


init : Flags -> ( Model, Cmd Msg )
init _ =
    ( { currentPage = EmptyPage
      , posts = []
      , visiblePosts = []
      , listOfListPosts = [ [] ]
      , users = []
      , comments = []
      , lastError = ""
      , subListIndex = 0
      }
    , Cmd.none
    )



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none



---- UPDATE ----


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        GetPosts ->
            ( { model | currentPage = LoadingPage }, getPosts )

        GotPosts (Ok posts) ->
            ( { model | posts = posts, currentPage = PostsPage, visiblePosts = posts, listOfListPosts = convertPostListToListofLists posts }
            , Cmd.none
            )

        GotPosts (Err error) ->
            ( { model | lastError = httpErrorString error }, Cmd.none )

        GetUsers ->
            ( { model | currentPage = UsersPage }, getUsers )

        GotUsers (Ok users) ->
            ( { model | users = users }
            , Cmd.none
            )

        GotUsers (Err error) ->
            ( { model | lastError = httpErrorString error }, Cmd.none )

        LoadPostComments postId ->
            ( model, getComments postId )

        GotPostComments (Ok comments) ->
            ( { model | comments = comments }, Cmd.none )

        GotPostComments (Err error) ->
            ( { model | lastError = httpErrorString error }, Cmd.none )

        IncrementPagination ->
            ( { model | subListIndex = model.subListIndex + 1 }, Cmd.none )

        DecrementPagination ->
            ( { model | subListIndex = model.subListIndex - 1 }, Cmd.none )

        _ ->
            ( model, Cmd.none )



---- HTTP STUFF


convertPostListToListofLists : List Post -> List (List Post)
convertPostListToListofLists list =
    let
        splitted =
            List.Extra.splitAt 5 list

        firstGroup =
            Tuple.first splitted

        rest =
            Tuple.second splitted
    in
    case List.length rest > 5 of
        True ->
            firstGroup :: convertPostListToListofLists rest

        False ->
            firstGroup :: rest :: []


httpErrorString : Http.Error -> String
httpErrorString error =
    case error of
        Http.BadBody message ->
            "Unable to handle response: " ++ message

        Http.BadStatus statusCode ->
            "Server error: " ++ String.fromInt statusCode

        Http.BadUrl url ->
            "Invalid URL: " ++ url

        Http.NetworkError ->
            "Network error"

        Http.Timeout ->
            "Request timeout"


getPosts : Cmd Msg
getPosts =
    Http.get
        { url = "https://jsonplaceholder.typicode.com/posts/"
        , expect = Http.expectJson GotPosts decodePosts
        }


decodePosts : Decode.Decoder (List Post)
decodePosts =
    Decode.list
        (Decode.succeed Post
            |> required "id" Decode.int
            |> required "userId" Decode.int
            |> required "title" Decode.string
            |> required "body" Decode.string
        )


decodeComments : Decode.Decoder (List Comment)
decodeComments =
    Decode.list
        (Decode.succeed Comment
            |> required "id" Decode.int
            |> required "postId" Decode.int
            |> required "name" Decode.string
            |> required "email" Decode.string
            |> required "body" Decode.string
        )


getComments : Int -> Cmd Msg
getComments postId =
    Http.get
        { url = "https://jsonplaceholder.typicode.com/comments?postId=" ++ String.fromInt postId
        , expect = Http.expectJson GotPostComments decodeComments
        }


decodeGeo : Decode.Decoder GeoCoords
decodeGeo =
    Decode.succeed GeoCoords
        |> required "lat" Decode.string
        |> required "lng" Decode.string


decodeAddress : Decode.Decoder Address
decodeAddress =
    Decode.succeed Address
        |> required "street" Decode.string
        |> required "suite" Decode.string
        |> required "city" Decode.string
        |> required "zipcode" Decode.string
        |> required "geo" decodeGeo


decodeCompany : Decode.Decoder Company
decodeCompany =
    Decode.succeed Company
        |> required "name" Decode.string
        |> required "catchPhrase" Decode.string
        |> required "bs" Decode.string


decodeUsers : Decode.Decoder (List User)
decodeUsers =
    Decode.list
        (Decode.succeed User
            |> required "id" Decode.int
            |> required "username" Decode.string
            |> required "name" Decode.string
            |> required "email" Decode.string
            |> required "phone" Decode.string
            |> required "website" Decode.string
            |> required "address" decodeAddress
            |> required "company" decodeCompany
        )


getUsers : Cmd Msg
getUsers =
    Http.get
        { url = "https://jsonplaceholder.typicode.com/users"
        , expect = Http.expectJson GotUsers decodeUsers
        }



---- VIEW ----


view : Model -> Browser.Document Msg
view model =
    let
        content =
            case model.currentPage of
                PostsPage ->
                    postsPage model

                LoadingPage ->
                    loadingPage

                _ ->
                    emptyPage
    in
    { title = "JSONPlaceholder browser"
    , body =
        [ layout [] <|
            column [ width fill, spacingXY 0 30 ]
                [ navBar
                , content
                ]
        ]
    }


emptyPage : Element Msg
emptyPage =
    column [] [ text "JSONPlaceholder browser" ]


loadingPage : Element Msg
loadingPage =
    row [ width fill, centerX ] [ text "Loading data..." ]


postsPage : Model -> Element Msg
postsPage model =
    row [ paddingXY 30 20, spacingXY 20 10, width fill ]
        [ el [] <| Input.button [] { label = el [centerX] <| text "Previous Page", onPress = Just DecrementPagination}
        , el [] <| Input.button [] { label = el [centerX] <| text "Next Page", onPress = Just IncrementPagination}
        , column [ paddingXY 20 20, spacingXY 20 10, width fill, alignLeft ] <| iteratePosts model
        , column [ paddingXY 20 20, spacingXY 20 10, width fill, alignTop ] <|
            List.map commentElement model.comments
        ]


iteratePosts : Model -> List (Element Msg)
iteratePosts model =
    let
        toRender =
            List.Extra.getAt model.subListIndex model.listOfListPosts
    in
    case toRender of
        Just posts ->
            List.map postElement posts

        Nothing ->
            [ none ]


postElement : Post -> Element Msg
postElement post =
    column [ pointer, Events.onClick <| LoadPostComments post.id ]
        [ el [ Font.bold ] <| text post.title
        , el [] <| text post.body
        ]


commentElement : Comment -> Element Msg
commentElement comment =
    column []
        [ el [] <| text comment.email
        , el [] <| text comment.name
        , el [] <| text comment.body
        ]


blue : Color
blue =
    rgb255 52 101 164


lightGreen : Color
lightGreen =
    rgb255 51 217 178


darkGreen : Color
darkGreen =
    rgb255 33 140 116


buttonInput : Msg -> String -> Color -> Color -> Element Msg
buttonInput callback btnText bgColor borderColor =
    el
        [ alignRight ]
    <|
        Input.button
            [ Background.color bgColor
            , Border.color borderColor
            , Border.rounded 3
            , Border.widthEach { bottom = 3, top = 0, right = 0, left = 0 }
            , Font.bold
            , Font.color <| rgb255 255 255 255
            , width (px 200)
            , height (px 40)
            ]
            { onPress = Just callback
            , label = el [ centerX ] <| text btnText
            }


navBar : Element Msg
navBar =
    row
        [ width fill
        , paddingXY 60 10
        , Border.widthEach { bottom = 1, top = 0, left = 0, right = 0 }
        , Border.color blue
        ]
        [ el [ alignLeft ] <| text "JSONPlaceholder browser"
        , row
            [ width fill
            , padding 20
            , spacing 20
            ]
            [ buttonInput GetPosts "Posts" lightGreen darkGreen
            , buttonInput GetUsers "Users" lightGreen darkGreen
            ]
        ]



---- PROGRAM ----


main : Program Flags Model Msg
main =
    Browser.document
        { view = view
        , init = init
        , update = update
        , subscriptions = subscriptions
        }
